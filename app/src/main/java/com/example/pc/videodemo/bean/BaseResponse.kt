package com.example.pc.videodemo.bean

/**
 *  @author: hyzhan
 *  @date:   2019/7/2
 *  @desc:   TODO
 */
data class BaseResponse(val code: Int, val err_message: String, val data: Intercom)

data class Intercom(val name: String)