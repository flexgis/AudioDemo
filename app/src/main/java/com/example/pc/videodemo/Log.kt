package com.example.pc.videodemo

import android.content.Context
import android.util.Log
import android.widget.Toast

/**
 *  @author: hyzhan
 *  @date:   2019/6/27
 *  @desc:   TODO
 */

fun String.showLog() {
    Log.d("ZZZZZZ", this)
}

fun Context.toast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}